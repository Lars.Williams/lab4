package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class BriansBrain implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		/** added a return of numrows */
		return this.currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		/** added a return of numcolumns */
		return this.currentGeneration.numColumns();
		
	}

	@Override
	public CellState getCellState(int row, int col) {
		/** added a return of the get function from cellstate */
		
		return this.currentGeneration.get(row, col);
	}

	
	
	/** adding a command to determine the state of neighboring cells*/
	private int howAreNeighbors(int row, int col, CellState state) {
		
		int alive = 0;
		int totalRows = numberOfRows();
		int totalColumns = numberOfColumns();

		for (int i = row-1; i<=row+1; i++) {
			
			for (int j = col-1; j<=col+1; j++) {
				
				
				
				
				if (i > totalRows || j > totalColumns) {
					continue;
				}
				
				if (i < 0 || j < 0) {
					continue;
				}
				
				if (i == row && j == col) {
					continue;
				}
				

				
				if(getCellState(i, j) == state) {
								
					alive = alive+1;

				}
			}
		}
		return alive;
	}
	
	
	
	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		int totalColumns= numberOfColumns();
		int totalRows= numberOfRows();
		int i=0;
		int j=0;
		
		
		for (i = 0; i < totalRows;){
			
			for (j = 0; j < totalColumns;) {
				
				nextGeneration.set(i, j, this.getNextCell(i, j));
				
				j++;
			}
			i++;
		}
		currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {

		int alive = howAreNeighbors(row, col, CellState.ALIVE);

		CellState now = getCellState(row, col);
		CellState next = now;

		
		if (now == CellState.ALIVE) {
			
				next = CellState.DYING;
			
		}

		else if (now == CellState.DYING) {
			
				next = CellState.DEAD;
			
		}
		
		else if (now == CellState.DEAD) {
			if(alive == 2) {
				next = CellState.ALIVE;
			}
		}
		return next;
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the borders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	
	/** found where i should have put the counting of neighbors function, feel silly*/
	private int countNeighbors(int row, int col, CellState state) {
		// TODO
		return 0;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
