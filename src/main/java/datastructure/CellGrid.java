package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
	
	private int columns;
	private int rows;
	private CellState[][] CellGrid;
	
	


    public CellGrid(int rows, int columns, CellState initialState) {
    	/** added "this" terms for rows columns, and for the cellstate value */
		this.rows = rows;
		this.columns = columns;
		this.CellGrid = new CellState[rows][columns];
		
		/** using i and j to represent rows and columns */
        for (int i = 0; i < rows; i++){
            for (int j = 0; j < columns; j++){

                this.CellGrid[i][j] = initialState;
            }
        }
		
	}

    @Override
    public int numRows() {

    	//returns integer row value
        return rows;
    }

    @Override
    public int numColumns() {

    	// returns integer column value
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
    	checkOutOfBounds(row, column);
        this.CellGrid[row][column]= element;
    
             
    }
       
    
    /** something is busted with the out of bounds exception when running gameoflife
     * will attempt separating the identification of out of bounds to find problem*/
    
    public void checkOutOfBounds(int row, int column) {
    	

        if (row >= this.rows || column >= this.columns) {

            throw new IndexOutOfBoundsException("larger than array");
        }

       	else if (row < 0 || column < 0) {

            throw new IndexOutOfBoundsException("negative value");
        }
        
    }
    
    

    @Override
    public CellState get(int row, int column) {

    	
    	System.out.println(column+" column   "+row+" row    ");
    	
    	checkOutOfBounds(row, column);
    	
    	

    	return CellGrid[row][column];
             

        
    }
    
    
    
    @Override
    public IGrid copy() {
		IGrid copyGrid = new CellGrid(this.rows, this.columns, CellState.DEAD);
		/** using i and j for the row and column values */
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				/** built superficial copy of grid */
	                copyGrid.set(i, j, this.get(i, j));
	        }
	    }
	    return copyGrid;
       
    }
    
}
